package maze;

import maze.exception.InvalidMazeTileStateException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MazeTileSpaceTest {

    public MazeTileSpaceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of canExplore method, of class MazeTileSpace.
     */
    @Test
    public void testCanExplore() {

        MazeTileSpace instance = new MazeTileSpace();
        boolean expResult = true;
        boolean result = instance.canExplore();
        assertEquals(expResult, result);
    }

    /**
     * Test of isExplored method, of class MazeTileSpace.
     */
    @Test
    public void testIsExplored() {

        MazeTileSpace instance = new MazeTileSpace();
        boolean expResult = false;
        boolean result = instance.isExplored();
        assertEquals(expResult, result);

    }

    /**
     * Test of shouldExplore method, of class MazeTileSpace.
     */
    @Test
    public void testShouldExplore() {

        MazeTileSpace instance = new MazeTileSpace();
        boolean expResult = true;
        boolean result = instance.shouldExplore();
        assertEquals(expResult, result);


    }

    /**
     * Test of setExplored method, of class MazeTileSpace.
     */
    @Test
    public void testSetExplored() throws InvalidMazeTileStateException {

        MazeTileSpace instance = new MazeTileSpace();
        instance.setExplored();
        char expResult = 'G';
        char result = instance.getDrawState();
        assertEquals(expResult, result);

    }

    /**
     * Test of setBadPath method, of class MazeTileSpace.
     *
     * @throws maze.exception.InvalidMazeTileStateException
     */
    @Test
    public void testSetBadPath() throws InvalidMazeTileStateException {

        MazeTileSpace instance = new MazeTileSpace();
        instance.setBadPath();
        char expResult = 'B';
        char result = instance.getDrawState();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDrawState method, of class MazeTileSpace.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetDrawState() throws Exception {
        MazeTileSpace instance = new MazeTileSpace();
        char expResult = 'U';
        char result = instance.getDrawState();
        assertEquals(expResult, result);

    }

}
