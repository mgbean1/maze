package maze;
import maze.exception.TryingToExploreBadMazeLocationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;

public class MazeTileVoidTest {
    
    public MazeTileVoidTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of canExplore method, of class MazeTileVoid.
     */
    @Test
    public void testCanExplore() {
        MazeTileVoid instance = new MazeTileVoid();
        boolean expResult = false;
        boolean result = instance.canExplore();
        assertEquals(expResult, result);

    }

    /**
     * Test of isExplored method, of class MazeTileVoid.
     */
    @Test
    public void testIsExplored() {
        MazeTileVoid instance = new MazeTileVoid();
        boolean expResult = false;
        boolean result = instance.isExplored();
        assertEquals(expResult, result);

    }

    /**
     * Test of shouldExplore method, of class MazeTileVoid.
     */
    @Test
    public void testShouldExplore() {
        MazeTileVoid instance = new MazeTileVoid();
        boolean expResult = false;
        boolean result = instance.shouldExplore();
        assertEquals(expResult, result);

    }

    /**
     * Test of setExplored method, of class MazeTileVoid.
     * @throws java.lang.Exception
     */
    @Test(expected = TryingToExploreBadMazeLocationException.class)
    public void testSetExplored() throws TryingToExploreBadMazeLocationException  {
        MazeTileVoid instance = new MazeTileVoid();
        instance.setExplored();
       
    }

    /**
     * Test of setBadPath method, of class MazeTileVoid.
     */
    @Test
    public void testSetBadPath() {
        MazeTileVoid instance = new MazeTileVoid();
        instance.setBadPath();

    }

    /**
     * Test of getDrawState method, of class MazeTileVoid.
     */
    @Test
    public void testGetDrawState() {
        MazeTileVoid instance = new MazeTileVoid();
        char expResult = 'W';
        char result = instance.getDrawState();
        assertEquals(expResult, result);

    }
    
}
