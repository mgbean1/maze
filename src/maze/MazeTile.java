package maze;

import maze.exception.InvalidMazeTileStateException;
import maze.exception.TryingToExploreBadMazeLocationException;

public abstract class MazeTile {
    /**
     * Can this tile be explored?
     *
     * @return
     */
    public abstract boolean canExplore();

    /**
     * Has this tile been explored before?
     *
     * @return
     */
    public abstract boolean isExplored();

    /**
     * Should this tile be explored?
     *
     * @return
     */
    public abstract boolean shouldExplore();

    /**
     * Set that this tile has been explored.
     *
     * @throws TryingToExploreBadMazeLocationException
     */
    public abstract void setExplored() throws TryingToExploreBadMazeLocationException;

    /**
     * Set that this tile is not a solution for the maze.
     */
    public abstract void setBadPath();

    /**
     * Return a char to describe what should be rendered. W = Wall, G = Solved
     * maze path, B = Bad maze path, U = Unexplored maze path
     *
     * @return
     * @throws InvalidMazeTileStateException
     */
    public abstract char getDrawState() throws InvalidMazeTileStateException;
}
