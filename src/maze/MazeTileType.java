package maze;

public enum MazeTileType {
    /**
     * For maze tiles of unknown origin
     */
    UNASSIGNED,
    /**
     * A wall, which cannot be explored
     */
    WALL,
    /**
     * A space, which can be explored
     */
    SPACE,
}
