package maze.exception;

/**
 * The provided maze was malformed.
 */
public class InvalidMazeTileException extends Exception {
    public InvalidMazeTileException() {
        super();
    }
}
