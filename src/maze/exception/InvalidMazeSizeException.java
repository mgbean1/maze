package maze.exception;

/**
 * Refusing to accept an improperly sized maze
 */
public class InvalidMazeSizeException extends Exception {
    public InvalidMazeSizeException() {
        super();
    }
}
