package maze.exception;

/**
 * Trying to get the render state of a tile which is in an unexpected status.
 */
public class InvalidMazeTileStateException extends Exception {
    public InvalidMazeTileStateException() {
        super();
    }
}
