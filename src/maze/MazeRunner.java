package maze;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import maze.exception.InvalidMazeSizeException;
import maze.exception.InvalidMazeTileException;
import maze.exception.NoMazeExitException;
import maze.exception.TryingToExploreBadMazeLocationException;

public class MazeRunner {
    /**
     * Reads, solves, and renders a maze file to a window.
     *
     * @param args Takes no arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MazeRenderer view = null;
            try {
                view = new MazeRenderer("MazeTestFile.txt");
            } catch (IOException ex) {
                Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidMazeSizeException ex) {
                Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidMazeTileException ex) {
                Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoMazeExitException ex) {
                Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TryingToExploreBadMazeLocationException ex) {
                Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
            }
            view.setVisible(true);
        });
    }
}
