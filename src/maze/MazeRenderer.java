package maze;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import maze.exception.InvalidMazeSizeException;
import maze.exception.InvalidMazeTileException;
import maze.exception.InvalidMazeTileStateException;
import maze.exception.NoMazeExitException;
import maze.exception.TryingToExploreBadMazeLocationException;

public class MazeRenderer extends JFrame {
    public MazeTile[][] maze;

    /**
     * Create the viewing window for GUI Title, size, location in window, close
     * condition
     *
     * @param filePath Path to a file containing maze data.
     * @throws IOException
     * @throws InvalidMazeSizeException
     * @throws InvalidMazeTileException
     * @throws NoMazeExitException
     * @throws TryingToExploreBadMazeLocationException
     */
    public MazeRenderer(String filePath) throws IOException, InvalidMazeSizeException, InvalidMazeTileException, NoMazeExitException, TryingToExploreBadMazeLocationException {
        maze = MazeReader.readMazeFileToArray(filePath);
        MazeSolver solver = new MazeSolver(maze);
        solver.solve();
        setTitle("Simple maze");
        setSize(1500, 1500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    /**
     * Drawing the array maze based on the input W,G,B,U
     *
     * @param g
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Interate through the array
        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[0].length; col++) {

                try {
                    // Instantiate Color
                    Color color;

                    // Switch case paints cell on a case basis
                    // cycling thrpough each cell
                    switch (maze[row][col].getDrawState()) {
                        case 'W':
                            color = Color.BLACK;
                            break;
                        case 'G':
                            color = Color.GREEN;
                            break;
                        case 'B':
                            color = Color.RED;
                            break;
                        case 'U':
                            color = Color.WHITE;
                            break;
                        default:
                            color = Color.WHITE;
                    }
                    // set color based on case
                    // set gridlines

                    g.setColor(color);
                    g.fillRect(100 + 30 * col, 100 + 30 * row, 30, 30);
                    g.setColor(Color.BLACK);
                    g.drawRect(100 + 30 * col, 100 + 30 * row, 30, 30);
                } catch (InvalidMazeTileStateException ex) {
                    Logger.getLogger(MazeRenderer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
