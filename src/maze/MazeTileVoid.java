package maze;

import maze.exception.TryingToExploreBadMazeLocationException;

/**
 * Intended for situations where an input maze is not a proper rectangle,
 * treated as a wall
 */
public class MazeTileVoid extends MazeTile {
    private final MazeTileType type;

    public MazeTileVoid() {
        this.type = MazeTileType.UNASSIGNED;
    }

    @Override
    public boolean canExplore() {
        return false;
    }

    @Override
    public boolean isExplored() {
        return false;
    }

    @Override
    public boolean shouldExplore() {
        return false;
    }

    @Override
    public void setExplored() throws TryingToExploreBadMazeLocationException {
        throw new TryingToExploreBadMazeLocationException();
    }

    @Override
    public void setBadPath() {

    }

    @Override
    public char getDrawState() {
        return 'W';
    }
}
